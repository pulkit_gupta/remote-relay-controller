/*

Author: Pulkit Gupta

R&D Head
*/

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;




public class Main  implements ActionListener,ItemListener
{
	private int led=18;
	JFrame f;                     //container n frame
	JPanel mp,mp1,mp2,mp3;
	JPanel p[] = new JPanel[led];


	//StatusBar statusbar = new StatusBar();
	JButton b[] = new JButton[led];            //components
	JLabel l1[] = new JLabel[led]	;
	Icon off= new ImageIcon("lioff.png");
	Icon on= new ImageIcon("liton.png");
	JMenuBar mb = new JMenuBar();
	JMenu file,settings;
	JMenuItem save;
	JCheckBoxMenuItem onall,offall,manual;
	JRadioButtonMenuItem pattern,monitor;




	String but[]=new String[]{"Pin1","Pin2","Pin3","Pin4","Pin5","Pin6","Pin7","Pin8","Pin9","Pin10","Pin11","Pin12","Pin13","Pin14","Pin15","Pin16","Pin17","Pin18"};  //logics
	boolean lab[] = new boolean[led];

	public void itemStateChanged(ItemEvent a)
	{
		if(onall.isSelected())
		{
			for(int i=0;i<led;i++)
			{
				l1[i].setIcon(on);
				lab[i]=true;
			}
		}
		else if(offall.isSelected())
		{
			for(int i=0;i<led;i++)
			{
				l1[i].setIcon(off);
				lab[i]=false;
			}
		}
	}

	public void actionPerformed(ActionEvent e)
	{

		if(e.getSource()==save)
		{
			JOptionPane.showMessageDialog(null, "updating");
		}
		else  //if push button is pressed
		{
			manual.setState(true);

			for(int i=0;i<led;i++)
			{
				if(e.getSource()==b[i])
				{
					lab[i]=!lab[i];
					if(lab[i])
					{
						l1[i].setIcon(on);
					}
					else
					{
						l1[i].setIcon(off);
					}
				}

			}
		}




	}

	public void createMenu()
	{

		file = new JMenu("File");
		file.setMnemonic(KeyEvent.VK_F);
		settings = new JMenu("Settings");

		save = new JMenuItem("Save",new ImageIcon("save.png"));
		save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		//save.setAccelerator(KeyStroke.getKeyStroke('S', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        save.addActionListener(this);

		file.add(save);
		file.addSeparator();
		ButtonGroup group = new ButtonGroup();

		pattern = new JRadioButtonMenuItem("Pattern Maker");
		group.add(pattern);
		file.add(pattern);

		monitor = new JRadioButtonMenuItem("Monitor");
		monitor.setSelected(true);
		group.add(monitor);
		file.add(monitor);

		//a group of check box menu items
		file.addSeparator();

		ButtonGroup group2 = new ButtonGroup();
		onall = new JCheckBoxMenuItem("All High",new ImageIcon("lon.png"));
		group2.add(onall);
		file.add(onall);

		offall= new JCheckBoxMenuItem("All Low",new ImageIcon("loff.png"));
		group2.add(offall);
		offall.setState(true);
		file.add(offall);

		manual = new JCheckBoxMenuItem("Manual");
		group2.add(manual);
		offall.setState(false);
		file.add(manual);

		onall.addItemListener(this);
		offall.addItemListener(this);

		mb.add(file);
		f.setJMenuBar(mb);

	}



	public Main()
	{
		f = new JFrame("pulkit");
		f.setIconImage(Toolkit.getDefaultToolkit().getImage("liton.png"));
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		createMenu();

		mp = new JPanel();
		mp1 = new JPanel();
		mp2 = new JPanel();
		mp3 = new JPanel();



		for(int i=0;i<led;i++)
		{
			p[i] = new JPanel();

			l1[i] = new JLabel(off);

			b[i] = new JButton(but[i]);
			b[i].addActionListener(this);
			p[i].add(l1[i]);     p[i].add(b[i]);
			if(i<6)
			mp1.add(p[i]);
			else if(i<12)
			mp2.add(p[i]);
			else if(i<18)
			mp3.add(p[i]);
		}
		mp1.setLayout(new BoxLayout(mp1,BoxLayout.Y_AXIS));
		mp2.setLayout(new BoxLayout(mp2,BoxLayout.Y_AXIS));
		mp3.setLayout(new BoxLayout(mp3,BoxLayout.Y_AXIS));
		mp.add(mp1);mp.add(mp2);mp.add(mp3);
		mp.setLayout(new BoxLayout(mp,BoxLayout.X_AXIS));











		f.add(mp,BorderLayout.CENTER);
		//f.getContentPane().add(statusbar,BorderLayout.SOUTH);




		f.setSize(959,584);        //position of frame
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension ssize = toolkit.getScreenSize();
    	f.setLocation((int)( ssize.getWidth() - f.getWidth() ) / 2,(int)( ssize.getHeight() - f.getHeight() ) / 2);
		//f.setResizable(false);
		f.setVisible(true);

		while(true)
		{
			//System.out.println(f.getWidth()+","+f.getHeight());
			//print(2);
		}

	}

	native public void print(int s);
	static
	{
		//System.loadLibrary("Hello");
	}



	public static void main(String args[])
	{
		Main t = new Main();



	}

}